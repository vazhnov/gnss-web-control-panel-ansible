#!/usr/bin/env bash
# SPDX-License-Identifier: MIT
set -o nounset
set -o errexit
set -o pipefail
shopt -s dotglob

# Take first argument OR default value 'lime2-legacy.local' (see `man bash`, "Parameter Expansion")
RemoteHost=${1:-lime2-legacy.local}

export ANSIBLE_HOST_KEY_CHECKING=False
export ANSIBLE_SSH_PIPELINING=True
export ANSIBLE_NO_TARGET_SYSLOG=True
ansible-playbook gnss-web-control.yaml --user=lenin --extra-vars 'ansible_port=2 RemoteAdminUser=lenin' --diff -i "${RemoteHost}",
