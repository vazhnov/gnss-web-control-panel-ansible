# If GNSS_DEVICE changed, update also `ConditionPathExists=` in systemd unit file with
# sudo systemctl edit gpspipe-nmea.service
# sudo systemctl edit gpspipe-raw.service

# When to start shutdown process. Used by `cron_check_battery_shutdown.sh`.
BATTERY_PERCENT_MIN='10'

# `NET_DEV` is using in `start_services.sh` to stop `gpspipe` if Ethernet cable is connected - home mode
NET_DEV='eth0'

# Name `gps0` is set in `/lib/udev/rules.d/60-gpsd.rules` and usually points to `/dev/ttyACM0`.
GNSS_DEVICE='/dev/gps0'
NMEA_LOG_DIR='/tmp/gpspipe'
NMEA_ARCHIVE_DIR='/var/log/gpspipe'
# To split files every 10000 lines, use: GPSPIPE_EXTRA_ARGS='--nmea -n 10000'
GPSPIPE_EXTRA_ARGS='--nmea'

# webgps: https://gitlab.com/gpsd/gpsd/-/blob/master/contrib/webgps.py.in
WEBGPS_WORKDIR='/tmp/webgps'
# Use `--no-html-head` if you want to include the result somewhere else
WEBGPS_ARGS='--no-strict-version-check c'
