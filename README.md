Ansible automation to set up GNSS tracks recorder + web interface + Wi-Fi hotspot to SBC [Olimex lime2](https://www.olimex.com/wiki/A20-OLinuXino-LIME2).

[Armbian](https://www.armbian.com/olimex-lime-2/) GNU/Linux distribution required.

## How to use

### Prepare Armbian image

Build your Armbian image (see [official documentation](https://github.com/armbian/build)):

```sh
apt-get -y install git
git clone 'https://github.com/armbian/build'
cd build
./compile.sh BOARD=lime2 BRANCH=legacy RELEASE=bullseye BUILD_MINIMAL=yes BUILD_DESKTOP=no KERNEL_ONLY=no KERNEL_CONFIGURE=prebuilt COMPRESS_OUTPUTIMAGE=sha,gpg,img HOST=lime2-legacy PACKAGE_LIST_EXCLUDE=smartmontools,nala PACKAGE_LIST=vim,zsh,avahi-daemon,libnss-mdns BOOTFS_TYPE=ext2
```

Some parameters:
* `BRANCH=legacy KERNEL_ONLY=no KERNEL_CONFIGURE=prebuilt` — use legacy pre-compiled kernel `linux-image-legacy-sunxi` (version 5.4.88-sunxi);
* `BOOTFS_TYPE=ext2` — also it is a switch to use separate boot partition, increases image size;
* `HOST=lime2-legacy` — a hostname. If empty, then: `[[ -z $HOST ]] && HOST="$BOARD" # set hostname to the board;`;
* package `avahi-daemon` is important to easily find your SBC in a local network, with name `hostname.local`;


Then:

* Write your image to microSD card.
* Mount microSD card root partition locally, copy your public SSH key(s) into `/root/.ssh/authorized_keys`.
* Unmount microSD card.

### Boot up SBC

* Plug microSD into SBC.
* Connect local network with Ethernet cable.
* Power on your SBC.

Check SSH from your desktop to SBC works:
```sh
ssh lime2-legacy.local
```

### Run Ansible

```sh
git clone 'git@gitlab.com:vazhnov/gnss-web-control-panel-ansible.git'
cd gnss-web-control-panel-ansible
cp -pv group_vars/all/wifi_password.yml.example group_vars/all/wifi_password.yml
```

Fill a password for a new Wi-Fi network:
```sh
editor group_vars/all/wifi_password.yml
```

Run Ansible in check mode:
```sh
bash ./start_check_diff.sh
```

Apply:
```sh
bash ./start_apply.sh
```

## Links

* https://gitlab.com/vazhnov/gnss-web-control-panel-ansible — this project at GitLab;
* https://gitlab.com/vazhnov/gnss-web-control — the website code (Python + Flask);
* [Record GNSS traces](https://wiki.openstreetmap.org/wiki/User:Vazhnov_Alexey/Record_GNSS_traces) + [Bluetooth and web panel](https://wiki.openstreetmap.org/wiki/User:Vazhnov_Alexey/Record_GNSS_traces/Bluetooth_and_web_panel) — manual setup process documented at https://wiki.openstreetmap.org;

## Copyright

MIT license.
